import requests
import time
import logging
import os
import base64
from otter_grader_service.common.payload import Submission

target_url = os.getenv("GRADER_PUSHER_URL")
token = os.getenv("IN_TOKEN")
base_dir = "data"
max_retries = 60
retry_interval = 10  # 10 seconds; 10 minutes total
log = logging.getLogger()


def read_archive(file_name):
    f = open(os.path.join(base_dir, file_name+".zip"), "rb")
    bin_buf = f.read()
    f.close()
    return base64.b64encode(bin_buf)


solution = read_archive("solution")

# Check if grading environment is ready
attempts = max_retries
while attempts > 0:
    try:
        response = requests.get(url=target_url)
        attempts = 0
    except Exception as e:
        log.warning(e)
        log.info(f'Connection failed, {attempts} attempts remaining.')
        attempts -= 1
        if attempts == 0:
            log.fatal("No connection to pusher, giving up")
            exit(1)
        time.sleep(retry_interval)

# Test submissions
for i in range(3):
    submission = Submission(
        solution=solution,
        submission=read_archive(f"submission_{i}"),
        submission_id=i+1,
        course_id=0
    )

    headers = {'access-token': token}
    response = requests.post(url=target_url, headers=headers, data=submission.model_dump_json())

