# Services for otter-grader-service Integration Tests

## otter-grader-test-in
Sends 3 submissions to the grading queue:

1. one containing no notebook
2. one containing one notebook
3. one containing two notebooks

Only the second should pass without error, the others should return error codes from
otter_grader_service.grading.worker
